var express = require('express')
var router = express.Router();
var path = require('path');
var authUtil = require('../services/authUtil');

var login = require('./login');
var Users = require('../models/users');
var register = require('./register');

router
  .use('/register', register)
  .use('/login',login)

  .get('/', function(req, res) {
    res.render('index');
  })

  .get('/dashboard', authUtil.requireLogin('/login'), function(req,res) {
    console.log('dashboard counter: ' + req.session.counter);
    res.render('dashboard', {counter: req.session.counter || 0});
  })

  .get('/logout', function(req, res) {
    if (req.session) {
      req.session.reset();
    }
    res.redirect('/');
  })

  .get('/adminpanel', authUtil.requirePermissions(['admin'],'/dashboard'), function(req,res) {
    Users.getAll(function(err,users) {
      res.render('adminpanel', {users:users});
    });
  })

  .get('/click', function(req,res) {
    req.session.counter = (req.session.counter+1) || 1;
    console.log('click counter: ' + req.session.counter);
    res.redirect('/dashboard');
  })

  .all('*',function(req,res) {
    res.redirect('/dashboard');
  });

module.exports = router;
